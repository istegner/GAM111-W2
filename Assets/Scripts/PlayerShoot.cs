﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

    [Tooltip("Uses local Up vector as the facing of the play field plane.")]
    public Transform playfieldOrigin;

    public void Shoot()
    {
        //where should I shoot
        var mouseScrPos = Input.mousePosition;
        var mouseRay = Camera.main.ScreenPointToRay(mouseScrPos);
        var playfieldPlane = new Plane(playfieldOrigin.up, playfieldOrigin.position);

        //use ray against playfield to determine where player would shoot
        float rayHitDist = -1;
        if (playfieldPlane.Raycast(mouseRay, out rayHitDist))
        {
            var rayHitPos = mouseRay.GetPoint(rayHitDist);

            Debug.DrawLine(transform.position, rayHitPos, Color.red);
        }
    }
}
