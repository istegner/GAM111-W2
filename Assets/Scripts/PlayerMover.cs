﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover
{
    public string horAxis, verAxis;
    public float moveForceScale = 1;
    public Rigidbody2D rb2d;

    public void MovePlayer()
    {
        //gather
        var movDir = new Vector2(Input.GetAxis(horAxis), Input.GetAxis(verAxis));
        if (movDir.magnitude > 1)
        {
            movDir.Normalize();
        }

        //apply
        rb2d.AddForce(movDir * moveForceScale);
    }
}
