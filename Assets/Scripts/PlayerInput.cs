﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public string horAxis, verAxis;

    public Vector2 GatherDesiredMovementVector()
    {
        //gather
        var movDir = new Vector2(Input.GetAxis(horAxis), Input.GetAxis(verAxis));
        if (movDir.magnitude > 1)
        {
            movDir.Normalize();
        }

        return movDir;
    }
}
